package pl.edu.pwsztar;

import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return Optional.ofNullable(id)
                .map(id -> id.length() == 11)
                .orElse(false);
    }

    @Override
    public Optional<Sex> getSex() {
        if(!isCorrect()) {
            return Optional.empty();
        }
        try {
            Sex sex = (Integer.parseInt(id.substring(9, 10)) % 2 == 0) ? Sex.WOMAN : Sex.MAN;
            return Optional.of(sex);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean isCorrect() {
        if(!isCorrectSize()) {
            return false;
        }
        try {
            int[] weights = {9,7,3,1,9,7,3,1,9,7};
            int sum = 0;
            for (int i = 0; i < 10; i++) {
                sum += Integer.parseInt(id.substring(i,i+1)) * weights[i];
            }
            sum %= 10;
            int controlNumber = Integer.parseInt(id.substring(10, 11));

            return (sum == controlNumber);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Optional<String> getDate() {
        if(!isCorrect()) {
            return Optional.empty();
        }
        try {
            String day = id.substring(4, 6);
            int month = Integer.parseInt(id.substring(2, 4));
            int year = Integer.parseInt(id.substring(0, 2));

            if (month > 80 && month < 93) {
                year += 1800;
                month -= 80;
            } else if (month > 0 && month < 13) {
                year += 1900;
            } else if (month > 20 && month < 33) {
                year += 2000;
                month -= 20;
            } else if (month > 40 && month < 53) {
                year += 2100;
                month -= 40;
            } else if (month > 60 && month < 73) {
                year += 2200;
                month -= 60;
            }
            String date = (month < 10) ? day + "-" + "0" + month + "-" + year : day + "-" + month + "-" + year;
            return (date.length() == 10) ? Optional.of(date) : Optional.empty();
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
