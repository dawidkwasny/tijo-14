package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check correct size"() {
        given:
            UserIdChecker userIdChecker = new UserId(userId)
        when:
            boolean isCorrect = userIdChecker.isCorrectSize()
        then:
            isCorrect == expect
        where:
            userId          ||  expect
            "88051272900"   ||  true
            "0"             ||  false
            "123456789"     ||  false
            "44051401458"   ||  true
    }

    @Unroll
    def "should check correct sex"() {
        given:
            UserIdChecker userIdChecker = new UserId(userId)
        when:
            Optional<UserIdChecker.Sex> sex = userIdChecker.getSex()
        then:
            sex.get() == expect
        where:
            userId          ||  expect
            "88051272412"   ||  UserIdChecker.Sex.MAN
            "44051401458"   ||  UserIdChecker.Sex.MAN
            "89031187203"   ||  UserIdChecker.Sex.WOMAN
    }

    @Unroll
    def "should check correct PESEL"() {
        given:
            UserIdChecker userIdChecker = new UserId("44051401458")
            UserIdChecker userIdChecker1 = new UserId("01123456789")
            UserIdChecker userIdChecker2 = new UserId("88051272412")
        when:
            def result = [userIdChecker.isCorrect(),
                          userIdChecker1.isCorrect(),
                          userIdChecker2.isCorrect()]
        then:
            result == [true,false,true]
    }

    @Unroll
    def "should return date"() {
        given:
            UserIdChecker userIdChecker = new UserId(userId)
        when:
            Optional<String> date = userIdChecker.getDate()
        then:
            date.get() == expect
        where:
            userId          ||  expect
            "44051401458"   ||  "14-05-1944"
            "88051272412"   ||  "12-05-1988"
    }
}
